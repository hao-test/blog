<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    //
    public function getIndex(){
    	return 'Hao';
    }
    public function store(UserRequest $request){
    	User::create([
    		'name' => $request->name,
    		'password' => Hash::make($request->password),
    		'email' => $request->email
    	]);
    	return '用戶註冊成功';
    }
    public function login(Request $request){
    	$res=Auth::guard('web')->attempt([
    		'name'=>$request->name,
    		'password'=> $request->password
    	]);
    	if($res){
    		return '用戶登入成功';
    	}
    	return '用戶登入失敗';
    }
}


<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Post;
use Session;
use App\Tag;
use App\Category;

class PagesController extends Controller {

	public function getIndex(){
		$posts = Post::orderBy('id','desc')->paginate(10);
		$tags = Tag::all();
		$categories = Category::all();
		return view('pages/welcome')->withPosts($posts)->withTags($tags)->withCategories($categories);
	}

	public function getAbout(){
		return view('pages/about');
	}

	public function getContact(){
		return view('pages/contact');
	} 

	public function postContact(Request $request){
		$this->validate($request,[
			'email' => 'required|email',
			'subject' => 'required',
			'message' => 'required']);

		$data = array(
			'email' => $request->email,
			'subject' => $request->subject,
			'bodyMessage' => $request->message
		);

		Mail::send('emails.contact',$data,function($message) use ($data){
			$message->from($data['email']);
			$message->to('gn09320408z@gmail.com');
			$message->subject($data['subject']);
		});

		Session::flash('success' , 'Email is send!');

		return redirect('/');
	}
}
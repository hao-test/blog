<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function tags(){
    	return $this->belongstomany('App\Tag');
    }

    public function comments(){
    	return $this->hasmany('App\Comment');
    }
}

<?php

namespace App\Test;

use App\Test\Adapter;

class Ship implements Adapter{
        
        protected $ship;

        public function __construct($ship){
            $this->ship = $ship;
        }
        public function getFee(){
            return $this->ship->FeeCal();
        }
    }
@extends('main')

@section('title','| Create New Post')

@section('content')

@section('stylesheets')

	{{ Html::style('css/select2.min.css') }}
	<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

	<script>
		tinymce.init({
			selector: "textarea", 
			menubar: false ,
			plugins: 'link',
			toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link'
		});
	</script>

@endsection


	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<h1>Create New Post</h1>
			<hr>

			{!! Form::open(array('route' => 'posts.store')) !!}
				{{ Form::label('title', 'Title:') }}
				{{ Form::text('title', null, array('class' => 'form-control', 'required' => '' )) }}

				{{ Form::label('slug', 'Slug:')  }}
				{{ Form::text('slug', null, array('class' => 'form-control' , 'required' => '' ))	}}

				{{ Form::label('category_id' , 'Category:') }}
				<select class="form-control" name="category_id">
					@foreach($categories as $category)
					<option value="{{ $category->id }}">{{ $category->name }}</option>
					@endforeach

				</select>
				{{ Form::label('tags' , 'Tags:') }}
				<select class="form-control select2-multi" name="tags[]" multiple="multiple">
					@foreach($tags as $tag)
					<option value="{{ $tag->id }}">{{ $tag->name }}</option>
					@endforeach
				</select>
				

				{{ Form::label('body', "Post Body:") }}
				{{ Form::textarea('body', null, array('class' => 'form-control' )) }}

				{{ Form::submit('新增', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!} 
			<a class="btn btn-danger btn-lg btn-block" href="/">取消</a>
		</div>
	</div>

@endsection

@section('scripts')


	{{ Html::style('js/select2.min.js') }}
	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection

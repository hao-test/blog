@extends('main')

@section('title',' | All Posts')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Posts</h1>
			<p>
			<form action="/posts" method="GET">
			<div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="title" class="form-control pull-right" placeholder="標題" value="">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-sm btn-outline-primary">尋找</button>
                </div>
             </div>
         	</form>
		</div>
		<div class="col-md-2">
			<br>
			<a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary">新增貼文</a>
		</div>
		<div class="col-md-12">
			<hr>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>標題</th>
					<th>內文</th>
					<th>類別</th>
					<th>建立時間</th>
					<th></th>
				</thead>

				<tbody>
					
					@foreach($posts as $post)

						<tr>
							<th>{{ $post->id }}</th>
							<td style="word-break:break-all; word-wrap:break-all;">{{ $post->title }}</td>
							<td style="word-break:break-all; word-wrap:break-all;">{{ substr(strip_tags($post->body),0,10) }} {{ strlen(strip_tags($post->body)) > 10 ? "..." : "" }}</td>
							@if($post->category)
							<td>{{ $post->category->name }}</td>
							@else
							<td></td>
							@endif
							<td >{{ date( 'm/j , Y', strtotime($post->created_at)) }}</td>
							<td><a href="{{ route('posts.show', $post->id) }}" class="btn btn-info ">查看</a> <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary ">編輯</a></td>
						</tr>

					@endforeach

				</tbody>
			</table>
			<div class="text-center">
				@if(Auth::user()->name == 'admin' && App\Post::count() >= 10)
					{{ $posts->links() }}
				@elseif( App\Post::where('username',Auth::user()->name)->count() >= 10 )
					{{ $posts->links() }}
				@else
				@endif
			</div>
		
		</div>
	</div>

@stop
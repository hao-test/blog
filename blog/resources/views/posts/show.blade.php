@extends('main')

@section('title', '| View Post')


@section('content')

	<div class="row">
		<div class="col-md-8">
			
			<h1 style="word-break:break-all; word-wrap:break-all;"> {{ $post->title }}</h1>			
			<p style="word-break:break-all; word-wrap:break-all;">{!! $post->body !!}</p>
			<hr>
			<div class="tags">
				@foreach($post->tags as $tag)
					<span class="label label-default">{{ $tag->name }}</span>
				@endforeach
			</div>
			
			<div id="backend-comments" style="margin-top: 50px;">
				<h3><small>{{ $post->comments()->count() }}</small>則留言</h3>

				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Comment</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($post->comments as $comment)
						<tr>
							<td>{{ $comment->name }}</td>
							<td>{{ $comment->email }}</td>
							<td>{{ $comment->comment }}</td>
							<td>
								<a href="{{ route('comments.edit' , $comment->id) }}" class="btn btn-primary btn-xs"><svg class="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"/>
  								<path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"/>
								</svg></a>
								<a href="{{ route('comments.delete' , $comment->id) }}" class="btn btn-danger btn-xs"><svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
								<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
								<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
								</svg></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			

		</div>

		<div class="col-md-4">
			
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Url:</dt>
					<dd><a href="{{ route('blog.single',$post->slug) }}">{{ url('blog/'.$post->slug) }}</a></dd>
				</dl>
				@if($post->category)
				<dl class="dl-horizontal">
					<dt>Category:</dt>
					<dd>{{ $post->category->name }}</dd>
				</dl>
				@endif
				<dl class="dl-horizontal">
					<dt>Created At:</dt>
					<dd>{{ date( 'Y-m-j  H:ia', strtotime($post->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date( 'Y-m-j  H:ia', strtotime($post->updated_at)) }}</dd>
				</dl>

				<hr>

				 <div class="row">

				 	<div class="col-sm-6">
				 		{!! Html::linkRoute('posts.edit','編輯', array($post->id) , array('class' => 'btn btn-primary btn-block')) !!}
				 		
				 	</div>
					
					<div class="col-sm-6">
						{!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE' ]) !!}
				 								
				 		{!! Form::submit('刪除', ['class'=>'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
				 	</div>				 	

				 </div>
				
				 		<p></p>
				 	
				 <div class="row">

				 	<div class="col-sm-12">
				 		{!! Html::linkRoute('posts.index','查看所有貼文' ,[],['class'=>'btn btn-secondary btn-block'])  !!}
				 	</div>
				 </div>

			</div>

		</div>
	</div>
	

@endsection
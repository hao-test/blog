@extends('main')

@section('title','| Homepage')

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="jumbotron">
            <h1>Welcome to My Blog!</h1>
            
            <p class="lead">Thanks for visiting my Blog</p>
            <p><a  class="btn btn-primary" href="/posts/create">新增貼文</a></p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-8">
          <div class="box">
          @foreach($posts as $post) 
          <div class="post">
            <h3 style="word-break:break-all; word-wrap:break-all;">{{ $post->title }}</h3>
            <p>建立時間：{{ date( 'Y-m-j  H:ia', strtotime($post->created_at)) }}</p>
            <p style="word-break:break-all; word-wrap:break-all;">{{ substr(strip_tags($post->body),0,50) }}{{ strlen(strip_tags($post->body)) > 50 ? "..." : "" }}</p>
            <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">查看</a>
          </div>
          <hr>
          @endforeach
          </div>
          <div class="text-center">
            @if(App\Post::count()>=10)
            {!!$posts->links()!!}
            @else
            @endif
          </div>
        </div>


        <div class="col-md-3 col-md-offset-1">
          <h5>Tags</h5>
          @foreach($tags as $tag)
          <li><a href="{{ route('tags.show', $tag->id) }}">{{ $tag->name }}</a></li>
          @endforeach
          <br>
          <h5>Categories</h5>
          @foreach($categories as $category)
          <li><a href="">{{ $category->name }}</a></li>
          @endforeach
        </div>
      </div>
@endsection
   

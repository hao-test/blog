@extends('main')

@section('title','| About')

@section('content')
        <div class="row">
            <div class="col-md-12">
            	<div style="text-align:center;"><h1>About Me</h1>
                <hr>
                想成為後端工程師的新鮮人，以Laravel為工具想加入後端的行業。<br>
				抱持著渴望以及熱情的態度，想了解後端的博大精深。<br>
				對於任何新事物都不懼怕困難及挑戰，會勇於面對並突破難關。
				</div>
            </div>
            
        </div>
        
@endsection

@section('sidebar')

@endsection
@extends('main')

@section('title' , '| Edit Comment')

@section('content')

	<h1>Edit Comment</h1>

	{{ Form::model($comment, ['route' => ['comments.update' ,$comment->id], 'method' => 'PUT']) }}

		{{ Form::label('name' , 'Name:') }}
		{{ Form::text('name' , null , ['class' => 'form-control' , 'disabled' => 'disabled' ]) }}

		{{ Form::label('email' , 'Email:') }}
		{{ Form::text('email' , null , ['class' => 'form-control' , 'disabled' => 'disabled' ]) }}

		{{ Form::label('comment' , 'Comment:') }}
		{{ Form::text('comment' , null , ['class' => 'form-control']) }}

		{{ Form::submit('更新留言', ['class' => 'btn btn-success' , 'style' => 'margin-top: 15px;']) }}

	{{ Form::close() }}

@endsection
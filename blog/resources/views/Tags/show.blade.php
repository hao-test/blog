@extends('main')

@section('title', "| $tag->name Tag")

@section('content')

	<div class="row">
		<div class="col-md-8">
			<br>
			<h1>{{ $tag->name }} Tag <small>{{ $tag->posts()->count() }} Posts </small></h1>
		</div>
		<div class="col-md-1">
			<br><br>
			<a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-sm btn-primary btn-block">編輯</a>
			<br>
		</div>
		<div class="col-md-1">
			<br><br>
			{{ Form::open(['route' => ['tags.destroy',$tag->id ], 'method' => 'DELETE']) }}
				{{ Form::submit('刪除', ['class' => 'btn btn-danger btn-sm btn-block ']) }}
			{{ Form::close() }}
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>標題</th>
						<th>Tags</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($tag->posts as $post)
					<tr>
						<th>{{ $post->id }}</th>
						<td>{{ $post->title }}</td>
						<td>
							@foreach($post->tags as $tag)
							<span class="label label-default"> {{ $tag->name }}</span>
							@endforeach
						</td>
						<td><a href="{{ route('posts.show' , $post->id) }}" class="btn btn-default btn-xs btn-primary ">查看</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection

